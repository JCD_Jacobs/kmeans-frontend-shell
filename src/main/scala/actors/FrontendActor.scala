package actors

import akka.actor._
import actors.protocols.Backend._
import com.typesafe.config.ConfigFactory

class FrontendActor() extends Actor {

  val config = ConfigFactory.load()
  val kMeansBackendPath = config.getString("kmeans.backendActor")
  val backendActor = context.actorSelection(kMeansBackendPath)

  def receive() = {
    case jm: JobMessage => {
      backendActor ! jm
    }
    case result: ResultMessage => {
      println("num passes: %d".format(result.optimizationPasses))
      println("phase1: %d ms".format(result.timeReadTransform))
      println("phase2: %d ms".format(result.timeTransmit))
      println("phase3: %d ms\n".format(result.timeCompute))

      for (vector <- result.clusterCenters) {
        for (value <- vector) {
          print("%f ".format(value))
        }
        print("\n")
      }

      sys.exit(0)
    }

    case e: ErrorMessage => {
      println(e.exception)
      sys.exit(1)
    }
  }

}