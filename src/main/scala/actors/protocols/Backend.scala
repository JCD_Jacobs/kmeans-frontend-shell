package actors.protocols

object Backend {
  case class JobMessage(numNodes: Int, numClusters: Int, threshold: Double, zScoreTransform: Boolean, inputFilePath: String, outputFilePath: String)

  case class ResultMessage(jobID: Int, clusterCenters: Seq[Seq[Double]], optimizationPasses: Int, deltaSum: Double, timeReadTransform: Long, timeTransmit: Long, timeCompute: Long)

  case class ErrorMessage(exception: Exception)
}
