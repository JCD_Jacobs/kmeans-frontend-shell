import actors.FrontendActor
import actors.protocols.Backend.JobMessage
import akka.actor.{ Props, ActorSystem }
import java.io.File
import java.util.Locale

object Frontend {
  Locale.setDefault(Locale.ENGLISH)

  val help = "Usage: ./shell-frontend -i filename -c # -t #\n" +
    "       -i filename    : file containing data to be clustered\n" +
    "       -c clusters    : number of clusters allowed\n" +
    "       -t threshold   : threshold value\n\n" +
    "       -p nproc       : number of threads (default:1)\n" +
    "       -z             : don't zscore transform data (default: false)\n"

  val actorSystem = ActorSystem("kmeans-shell-frontend")
  val frontendActor = actorSystem.actorOf(Props[FrontendActor], name = "frontendActor")

  def main(args: Array[String]): Unit = {
    var argID = 0
    val lastID = args.length - 1
    var filename = ""
    var cluster = -1
    var threshold = -1.0
    var nproc = 1
    var zscore = true

    while (argID <= lastID) {

      args(argID) match {
        case "-i" => {
          filename = args(argID + 1)
          argID += 2
        }
        case "-c" => {
          cluster = args(argID + 1).toInt
          assert(cluster > 0)
          argID += 2
        }
        case "-t" => {
          threshold = args(argID + 1).toDouble
          argID += 2
        }
        case "-p" => {
          nproc = args(argID + 1).toInt
          assert(nproc > 0)
          argID += 2
        }
        case "-z" => {
          zscore = false
          argID += 1
        }
      }
    }

    if (filename.length > 0 && cluster > 0 && threshold > 0.0) {
      assert(new File(filename).exists(), "File not found: %s".format(filename))
      frontendActor ! JobMessage(nproc, cluster, threshold, zscore, filename, "")
    } else {
      println(help)
      System.exit(64)
    }
  }
}
