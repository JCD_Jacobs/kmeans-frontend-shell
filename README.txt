Requirements
============
* Java Runtime Environment
* Java Development Kit
* Scala Build Tool (sbt)


Configuration
=============
The configuration is in src/main/recources/application.conf

If you want to run kmeans on multiple computers, make shure akka.remote.netty.tcp.hostname and
akka.cluster.seed-nodes are correct.


Run
===
compile:

        $sbt assembly

run:

        $./shell-frontend -i filename -c # -t #

        -i filename    : file containing data to be clustered
        -c clusters    : number of clusters allowed
        -t threshold   : threshold value

        -z             : don't zscore transform data (default: false)
        -p nproc       : number of threads (default:1)


Firewall
========
This application listens on port 2552.